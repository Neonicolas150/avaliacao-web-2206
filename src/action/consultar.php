<?php
    require_once("../database/configuration/config.php");

    $filter = $_REQUEST["filter"];

    try {
        $connection = new PDO($dsn, $username, $password);

        if($filter == "Todos") {
            echo "
                <div id='cadastro'>
                    <a id='cadastro-todos' class='btn btn-primary p-2 mb-2' style='box-shadow: none !important;
                    border-color: none !important;' href='./pages/cadastro/relCadastro'>Cadastrar um relacionamento</a>
                </div>";
            // $sql = "SELECT r.id, c.nome, aluno.nome, turma.serie FROM curso c JOIN relacionamento r ON c.id = r.curso JOIN aluno ON aluno.matricula = r.aluno JOIN turma ON turma.serie = r.turma";

            $sql = "SELECT r.id, c.nome, a.nome, t.serie FROM relacionamento r INNER JOIN curso c ON r.curso = c.id INNER JOIN aluno a ON r.aluno = a.matricula INNER JOIN turma t ON r.turma = t.id";

        } else if($filter == "Alunos") {
            echo "
                <div id='cadastro'>
                    <a id='cadastro-alunos' class='btn btn-primary p-2 mb-2' style='box-shadow: none !important;
                    border-color: none !important;' href='./pages/cadastro/alunosCadastro.php'>Cadastrar aluno</a>
                </div>";
            $sql = "SELECT * FROM aluno";
        } else if($filter == "Turmas") {
            echo "
                <div id='cadastro'>
                    <a id='cadastro-turmas' class='btn btn-primary p-2 mb-2' style='box-shadow: none !important;
                    border-color: none !important;' href='./pages/cadastro/turmasCadastro'>Cadastrar turma</a>
                </div>";
            $sql = "SELECT * FROM turma";
        } else if($filter == "Cursos") {
            echo "
                <div id='cadastro'>
                    <a id='cadastro-cursos' class='btn btn-primary p-2 mb-2' style='box-shadow: none !important;
                    border-color: none !important;' href='./pages/cadastro/cursosCadastro'>Cadastrar curso</a>
                </div>";
            $sql = "SELECT * FROM curso";
        }

        $statement = $connection->prepare($sql);
        $statement->execute();

        $result = $statement->fetchAll();
 
        echo "<table class='table table-dark table-hover'>";
        if($statement->rowCount()){
            echo "<thead>";
            if($filter == "Todos") {
                echo "
                    <tr>
                        <th scope='col' class='p-2 text-center'>ID DO RELACIONAMENTO</th>
                        <th scope='col' class='p-2 text-center'>CURSO</th>
                        <th scope='col' class='p-2 text-center'>ALUNO</th>
                        <th scope='col' class='p-2 text-center'>TURMA</th>
                        <th scope='col' class='p-2 text-center'>EDITAR</th>
                        <th scope='col' class='p-2 text-center'>EXCLUIR</th>
                    </tr>
                ";
            } else if($filter == "Alunos") {
                echo "
                    <tr>
                        <th scope='col' class='p-2 text-center'>MATRÍCULA</th>
                        <th scope='col' class='p-2 text-center'>NOME</th>
                        <th scope='col' class='p-2 text-center'>EMAIL</th>
                        <th scope='col' class='p-2 text-center'>EDITAR</th>
                        <th scope='col' class='p-2 text-center'>EXCLUIR</th>
                    </tr>
                ";
            } else if($filter == "Turmas") {
                echo "
                    <tr>
                        <th scope='col' class='p-2 text-center'>ID</th>
                        <th scope='col' class='p-2 text-center'>SÉRIE</th>
                        <th scope='col' class='p-2 text-center'>EDITAR</th>
                        <th scope='col' class='p-2 text-center'>EXCLUIR</th>
                    </tr>
                ";
            } else if($filter == "Cursos") {
                echo "
                    <tr>
                        <th scope='col' class='p-2 text-center'>ID</th>
                        <th scope='col' class='p-2 text-center'>NOME</th>
                        <th scope='col' class='p-2 text-center'>COORDENADOR</th>
                        <th scope='col' class='p-2 text-center'>EDITAR</th>
                        <th scope='col' class='p-2 text-center'>EXCLUIR</th>
                    </tr>";
            }
                
            echo "</thead>";
                
            foreach($result as $row) {
                echo "<tbody>";
                if($filter == "Todos") {
                    echo "
                        <tr>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['id']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['1']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['2']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['serie']."</td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='./pages/editar/relacionamento.php?id=".$row['id']."' class='btn btn-warning p-1' title='Editar este relacionamento'><i class='fas fa-edit p-2'></a></td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='../src/action/remover/relacionamento.php?id=".$row['id']."' class='btn btn-danger p-1' title='Exluir este relacionamento'><i class='fas fa-trash p-2'></i></a></td>
                        </tr>
                    ";
                } else if($filter == "Alunos") {
                    echo "
                        <tr>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['matricula']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['nome']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['email']."</td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='./pages/editar/alunos.php?matricula=".$row['matricula']."' class='btn btn-warning p-1' title='Editar este perfil de aluno'><i class='fas fa-edit p-2'></a></td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='../src/action/remover/alunos.php?matricula=".$row['matricula']."' class='btn btn-danger p-1' title='Exluir este perfil de aluno'><i class='fas fa-trash p-2'></i></a></td>
                        </tr>
                    ";
                } else if($filter == "Turmas") {
                    echo "
                        <tr>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['id']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['serie']."</td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='./pages/editar/turmas.php?id=".$row['id']."' class='btn btn-warning p-1' title='Editar esta turma'><i class='fas fa-edit p-2'></a></td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='../src/action/remover/turmas.php?id=".$row['id']."' class='btn btn-danger p-1' title='Exluir esta turma'><i class='fas fa-trash p-2'></i></a></td>
                        </tr>
                    ";
                } else if($filter == "Cursos") {
                    echo "
                        <tr>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['id']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['nome']."</td>
                            <td scope='row' class='p-2 text-center align-middle'>".$row['coordenador']."</td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='./pages/editar/cursos.php?id=".$row['id']."' class='btn btn-warning p-1' title='Editar este curso'><i class='fas fa-edit p-2'></a></td>
                            <td scope='row' class='p-2 text-center align-middle'><a href='../src/action/remover/cursos.php?id=".$row['id']."' class='btn btn-danger p-1' title='Exluir este curso'><i class='fas fa-trash p-2'></i></a></td>
                        </tr>
                    ";
                }
                echo "</tbody>";
                }
            
            } else {
                echo "
                    <div id='sem-registro' class='bg-danger text-light p-3'>
                        <p>Ainda não existe nenhum registro no Banco de Dados!</p>
                    </div>
                ";
            }
            echo "</table>";

    } catch(PDOException $error) {
        echo "
                <div id='erro' class='bg-danger text-light p-3'>
                    <p>Ocorreu um erro no Banco de Dados, tente novamente.</p>
                    <code>".$error."</code>
                </div>
            ";
    }

?>