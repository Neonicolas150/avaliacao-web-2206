CREATE SCHEMA IF NOT EXISTS avaliacao;

CREATE TABLE IF NOT EXISTS avaliacao.curso(
    id INT AUTO_INCREMENT,
    nome VARCHAR(10) NOT NULL UNIQUE,
    coordenador VARCHAR(10) NOT NULL,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS avaliacao.turma(
    id INT AUTO_INCREMENT,
    serie INT NOT NULL UNIQUE,
    PRIMARY KEY(id)
);

CREATE TABLE IF NOT EXISTS avaliacao.aluno(
    matricula INT NOT NULL,
    nome VARCHAR(20) NOT NULL,
    email VARCHAR(30) NOT NULL UNIQUE,
    PRIMARY KEY(matricula)
);

CREATE TABLE IF NOT EXISTS avaliacao.relacionamento(
    id INT AUTO_INCREMENT,
    curso INT NOT NULL,
    aluno INT NOT NULL,
    turma INT NOT NULL,
    PRIMARY KEY(id),
    FOREIGN KEY(curso) REFERENCES curso(id),
    FOREIGN KEY(turma) REFERENCES turma(id),
    FOREIGN KEY(aluno) REFERENCES aluno(matricula)
);