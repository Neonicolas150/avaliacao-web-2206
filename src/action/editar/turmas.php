<?php
    require_once("../../database/configuration/config.php");
    require_once("../../../public/pages/template/header.php");

    $ID = $_REQUEST['id'];
    $SERIE = $_POST["serie"];
    
    try {
        $connection = new PDO($dsn, $username, $password);

        $query = "UPDATE turma SET serie = :s WHERE id = :id";

        $statement = $connection->prepare($query);

        $statement->bindValue(":s", $SERIE);
        $statement->bindValue(":id", $ID);

        if($statement->execute()) {
            echo "
                <div id='sucesso' class='bg-success text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>A edição da série foi efetivada com sucesso!</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
            header("refresh:2, ../../../public/home.php");
        } else {
            echo "
                <div id='sem-sucesso' class='bg-danger text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>A edição da série falhou, tente novamente.</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>

            ";
        }

    } catch(PDOException $error) {
        echo "
                <div id='erro' class='bg-danger text-light p-3'>
                    <p>Ocorreu um erro no Banco de Dados, tente novamente.</p>
                    <code>".$error."</code>
                </div>
            ";
    }

    require_once("../../../public/pages/template/footer.php");
?>