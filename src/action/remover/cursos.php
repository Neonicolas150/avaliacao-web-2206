<?php
    require_once("../../database/configuration/config.php");
    require_once("../../../public/pages/template/header.php");

    $ID = $_REQUEST['id'];

    try {
        $connection = new PDO($dsn, $username, $password);

        $query = "DELETE FROM curso WHERE id = :id";

        $statement = $connection->prepare($query);

        $statement->bindValue(":id", $ID);

        if($statement->execute()) {
            echo "
                <div id='sucesso' class='bg-success text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>O curso foi excluído com sucesso.</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-primary p-2' style='width: 100px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Voltar</a>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
            header("refresh:2, ../../../public/home.php");
        } else {
            echo "
                <div id='sem-sucesso' class='bg-danger text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>O curso não foi excluído.</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-primary p-2' style='width: 100px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Voltar</a>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
        }

    } catch(PDOException $error) {
        echo "
                <div id='erro' class='bg-danger text-light p-3'>
                    <p>Ocorreu um erro no Banco de Dados, tente novamente.</p>
                    <code>".$error."</code>
                </div>
            ";
    }


    require_once("../../../public/pages/template/footer.php");
?>