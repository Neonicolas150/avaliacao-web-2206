import { Ajax } from "./ajax.js";

const ajax = new Ajax();

let showResult = document.getElementById("show-result");

let defaultFilter = "Todos";
getSearch(defaultFilter);

let showTodos = document.querySelector("#todos");
let showAlunos = document.querySelector("#alunos");
let showCursos = document.querySelector("#cursos");
let showTurmas = document.querySelector("#turmas");

// Código para mudar o item selecionado no Frontend

var buttonActived = [
    {
        name: showTodos,
        click: showTodos.onclick = () => {
            defaultFilter = "Todos";
            addActiveClass(showTodos, 0);
            getSearch(defaultFilter);
        },
        actived: true
    },
    {
        name: showAlunos,
        click: showAlunos.onclick = () => {
            defaultFilter = "Alunos";
            console.log(defaultFilter)
            addActiveClass(showAlunos, 1);
            getSearch(defaultFilter);
        },
        actived: false
    },
    {
        name: showCursos,
        click: showCursos.onclick = () => {
            defaultFilter = "Cursos";
            addActiveClass(showCursos, 2);
            getSearch(defaultFilter);
        },
        actived: false
    },
    {
        name: showTurmas,
        click: showTurmas.onclick = () => {
            defaultFilter = "Turmas";
            addActiveClass(showTurmas, 3);
            getSearch(defaultFilter);
        },
        actived: false
    },
]


function addActiveClass(selector, array) {
    Object.values(buttonActived).forEach((element) => {
        if(element.actived == true) {
            element.name.classList.remove("active");
            console.log(`Foi encontrado um elemento ativado.`);
        }
    })
    selector.classList.add("active");
    buttonActived[array].actived = true;
}

// Código para mostrar o resultado do item selecionado


function getSearch(filter) {
    try {
        console.log("Requisitando tabela...")
        ajax.requisitar(showResult, filter);
    } catch(err) {
        console.log(`Um erro inesperado ocorreu! ${err.getMessagem()}`);
    }   
}
