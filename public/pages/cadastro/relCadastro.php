<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cadastro de relacionamento</title>

    <!-- Incluindo o Bootstrap na aplicação -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
</head>
<body style="height: 100vh;width: 100vw;">
    <main id="cadastro-relacionamento" class="w-100 h-100 d-flex justify-content-center align-items-center flex-column">
        <div id="content">
            <div id="title" class="w-100 d-flex justify-content-start">
                <h1 class="mb-3">Cadastro de relacionamento</h1>
            </div>    
            <form action="../../../src/action/cadastrar/relacionamento.php" method="POST">
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="curso-input" name="curso" placeholder="Curso">
                    <label for="curso-input">Nome do curso</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="text" class="form-control" id="aluno-input" name="aluno" placeholder="Aluno">
                    <label for="aluno-input">Nome do aluno</label>
                </div>
                <div class="form-floating mb-3">
                    <input type="number" class="form-control" id="turma-input" name="turma" placeholder="Turma">
                    <label for="turma-input">Número da turma</label>
                </div>
                <div class="w-100 d-flex flex-row">
                    <a class="btn btn-outline-secondary w-50 me-3" href="../../home.php">Home</a>
                    <input class="btn btn-primary w-50 ms-3" type="submit" value="Cadastrar"> 
                </div>
            </form>
        </div>  
    </main>
</body>
</html>