<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <title>Avaliação de Laboratório Web 22/06</title>

        <!-- Incluindo estilos CSS customizados na aplicação -->
        <link rel="stylesheet" href="styles/home.css">

        <!-- Incluindo o Bootstrap na aplicação -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous"></script>
        
        <!-- Incluindo o Font Awesome na aplicação -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.2/css/all.min.css" rel="stylesheet">
    </head>
    <body>
        <div id="title" class="p-2 bg-dark text-light">
            <h1>Avaliação de Laboratório Web</h1>
        </div>
        <header class="p-2 pb-3 d-flex flex-row bg-dark">
            <div class="dropdown me-2 h-100">
                <button class="btn btn-primary dropdown-toggle h-100 p-2" id="" data-bs-toggle="dropdown" aria-expanded="false" type="button">
                    Listar
                </button>
                <ul class="dropdown-menu" aria-labelledby="filterButton">
                    <li><button class="dropdown-item active p-2" id="todos">Todos</button></li>
                    <li><button class="dropdown-item p-2" id="alunos">Alunos</button></li>
                    <li><button class="dropdown-item p-2" id="cursos">Cursos</button></li>
                    <li><button class="dropdown-item p-2" id="turmas">Turmas</button></li>
                </ul>  
            </div> 
            <div id="create-database">  
                <a class="btn btn-secondary p-2" href="../src/database/configuration/createDatabase.php">Criar banco de dados</a>
            </div>
        </header>  
        <main id="show-result" class="p-2">
            
        </main>   
        
        <script type="module" src="js/ajax.js"></script>
        <script type="module" src="js/app.js"></script>
    </body>
</html>