<?php
    require_once("../../database/configuration/config.php");
    require_once("../../../public/pages/template/header.php");

    $ID = $_REQUEST['id'];
    $CURSO = $_POST["curso"];
    $ALUNO = $_POST["aluno"];
    $TURMA = $_POST["turma"];

    try {
        $connection = new PDO($dsn, $username, $password);

        $query = "UPDATE relacionamento SET curso = (SELECT id FROM curso WHERE nome = :nomecurso), aluno = (SELECT matricula FROM aluno WHERE nome = :nomealuno), turma = (SELECT id FROM turma WHERE serie = :serie) WHERE id = :id";

        $statement = $connection->prepare($query);

        $statement->bindValue(":nomecurso", $CURSO);
        $statement->bindValue(":nomealuno", $ALUNO);
        $statement->bindValue(":serie", $TURMA);
        $statement->bindValue(":id", $ID);
        

        if($statement->execute()) {
            echo "
                <div id='sucesso' class='bg-success text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>A edição deste relacionamento foi feito com sucesso!</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
            header("refresh:2, ../../../public/home.php");
        } else {
            echo "
                <div id='sem-sucesso' class='bg-danger text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>A edição deste relacionamento falhou, tente novamente.</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>

            ";
        }

    } catch(PDOException $error) {
        echo "
                <div id='erro' class='bg-danger text-light p-3'>
                    <p>Ocorreu um erro no Banco de Dados, tente novamente.</p>
                    <code>".$error."</code>
                </div>
            ";
    }

    require_once("../../../public/pages/template/footer.php");
?>