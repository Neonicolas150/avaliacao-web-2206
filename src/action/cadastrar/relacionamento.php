<?php
    require_once("../../database/configuration/config.php");
    require_once("../../../public/pages/template/header.php");

    $CURSO = $_POST["curso"];
    $ALUNO = $_POST["aluno"];
    $TURMA = $_POST["turma"];

    try {
        $connection = new PDO($dsn, $username, $password);

        $query = "INSERT INTO relacionamento(curso, aluno, turma) VALUES((SELECT id FROM curso WHERE nome = :nomecurso),(SELECT matricula FROM aluno WHERE nome = :nomealuno),(SELECT id FROM turma WHERE serie = :serie))";

        $statement = $connection->prepare($query);

        $statement->bindValue(":nomealuno", $ALUNO);
        $statement->bindValue(":nomecurso", $CURSO);
        $statement->bindValue(":serie", $TURMA);


        if($statement->execute()) {
            echo "
                <div id='sucesso' class='bg-success text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>O relacionamento foi cadastrado com sucesso!</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-primary p-2' style='width: 100px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/pages/cadastro/relCadastro.php'>Voltar</a>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
            header("refresh:2, ../../../public/relCadastro.php");
        } else {
            echo "
                <div id='sem-sucesso' class='bg-danger text-light p-3'>
                    <p style='margin-bottom: 0 !important;'>O relacionamento não foi cadastrado com sucesso!</p>
                </div>
                <div id='action-button' class='mt-2'>
                    <a class='btn btn-primary p-2' style='width: 100px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/pages/cadastro/relCadastro.php'>Voltar</a>
                    <a class='btn btn-outline-primary p-2' style='width: 80px;box-shadow: none !important;
                    border-color: none !important;' href='../../../public/home.php'>Home</a>    
                </div>
            ";
        }

    } catch(PDOException $error) {
        echo "
                <div id='erro' class='bg-danger text-light p-3'>
                    <p>Ocorreu um erro no Banco de Dados, tente novamente.</p>
                    <code>".$error."</code>
                </div>
            ";
    }

    require_once("../../../public/pages/template/footer.php");

?>