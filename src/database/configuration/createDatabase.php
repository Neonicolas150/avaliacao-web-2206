<?php

    require("config.php");

    try {

        $connection = new PDO("mysql:host=$host", $username, $password);
        $sql = file_get_contents("../data/init.sql");
        
        
        if($connection->exec($sql)) {
            echo "O Banco de Dados e suas Tabelas foram criadas com sucesso!";
        } else {
            echo "Não foi possivel criar o Banco de Dados, tente novamente.";
        }

    } catch(PDOException $err) {
        echo $sql . "Ocorreu um erro na conexão com o banco de dados, tente novamente<br>" . $err->getMessage();
    }

?>